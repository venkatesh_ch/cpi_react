import './App.css';
import React from "react";
import ReactDOM from "react-dom";
// import { FaGem } from "react-icons/fa";
// import { FaHeart } from "react-icons/fa";

// import { slide as Menu } from 'react-burger-menu'

import ResponsiveDrawer from './components/sidebar'
import { Container } from '@material-ui/core';

import NewProgramComponent from './components/programs/new_program'
import Evaluation from './components/evaluations/evaluation'




function App() {
  return (
    <div className="App">
      <ResponsiveDrawer
        content = {
        <Container disableGutters = {true} maxWidth= {false}>
          {/* <NewProgramComponent/> */}
          <Evaluation/>
        </Container>
      }
      />

    </div>
  );
}

export default App;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Tooltip from '@material-ui/core/Tooltip';
import PropTypes from 'prop-types';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
// import ViewSlider from 'react-view-slider';
// import AwesomeSlider from 'react-awesome-slider';
// import 'react-awesome-slider/dist/styles.css';
// import Slider1 from 'react-perfect-slider';
// import Button from '@material-ui/core/Button';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
// import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
// import PersonIcon from '@material-ui/icons/Person';
// import AddIcon from '@material-ui/icons/Add';
// import Typography from '@material-ui/core/Typography';
// import { blue } from '@material-ui/core/colors';
// import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import Pagination from '@material-ui/lab/Pagination';

import Slide from '@material-ui/core/Slide';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Collapse from '@material-ui/core/Collapse';





const useStyles = makeStyles((theme) => ({
  root: {
  },
  margin: {
    // height: theme.spacing(3),
  },
}));

const marks = [
  {
    value: 0,
    label: '0',
  },
  {
    value: 1,
    label: '1',
  },
  {
    value: 2,
    label: '2',
  },
  {
    value: 3,
    label: '3',
  },
  {
    value: 4,
    label: '4',
  },
  {
    value: 5,
    label: '5',
  },
  {
    value: 6,
    label: '6',
  },
  {
    value: 7,
    label: '7',
  },
  {
    value: 8,
    label: '8',
  },
  {
    value: 9,
    label: '9',
  },
  {
    value: 10,
    label: '10',
  },
  {
    value: 11,
    label: '11',
  },
  {
    value: 12,
    label: '12',
  },
  {
    value: 13,
    label: '13',
  },
  {
    value: 14,
    label: '14',
  },
  {
    value: 15,
    label: '15',
  },
  {
    value: 16,
    label: '16',
  },
  {
    value: 17,
    label: '17',
  },
  {
    value: 18,
    label: '18',
  },
  {
    value: 19,
    label: '19',
  },
  {
    value: 20,
    label: '20',
  },


];

function valuetext(value) {
  console.log(window.innerWidth)
  if(value >= 20){
    return `Beyond Entry Level -  ${value}`;
  }
  if(value >= 16){
    return `Entry Level -  ${value}`;
  }
  if(value >= 12){
    return `Advanced Intermediate -  ${value}`;
  }
  if(value >= 8){
    return `Intermediate -  ${value}`;
  }
  if(value >= 4){
    // return `Advanced Beginner -  ${value} \n A student who requires clinical supervision less than 25% of the time managing new patients or patients with complex conditions and is independent managing patients with simple conditions. At this level, the student is consistent and proficient in simple tasks and requires only occasional cueing for skilled examinations, interventions, and clinical reasoning. The student is capable of maintaining 75% of a full-time physical therapist's caseload.`;
    return (<pre className="pre">{`Advanced Beginner -  ${value} \n A student who requires clinical supervision less than 25% of the time managing new patients or patients with complex conditions and is independent managing patients with simple conditions. At this level, the student is consistent and proficient in simple tasks and requires only occasional cueing for skilled examinations, interventions, and clinical reasoning. The student is capable of maintaining 75% of a full-time physical therapist's caseload.`}</pre>)
  }
  if(value >= 0){
    return `Beginner -  ${value}`;
  }
}

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

ValueLabelComponent.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired,
};

function Evaluation() {
  const classes = useStyles();

  const [allValues, setShowContent] = React.useState({
    showContent: false,
    showContent1: false,
    showContent2: false,
    showContent3: false,
    showContent4: false,
    showContent5: false
  });
  const onClickSetShowContent = (key) => {
    setShowContent({...allValues, [key]: !allValues[key]})
  }
  const [allValues1, setShowContent1] = React.useState({
    showContent: false,
    showContent1: false,
    showContent2: false,
    showContent3: false,
    showContent4: false,
    showContent5: false,
    showContent6: false,
    showContent7: false,
    showContent8: false,
    showContent9: false,
    showContent10: false,
    showContent11: false,
    // showContent12: false,
  });
  const onClickSetShowContent1 = (key) => {
    setShowContent1({...allValues1, [key]: !allValues1[key]})
  }

  const section2 = {
    showContent: {
      name: "Patient Management – Clinical Reasoning",
      question: "7. Applies current knowledge, theory, clinical judgment, and the patient’s values and perspective in patient management.",
    },
    showContent1: {
      name: "Patient Management – Screening",
      question: "8. Determines with each patient encounter the patient's need for further examination or consultation by a physical therapist or referral to another health care professional.",
    },
    showContent2: {
      name: "Patient Management – Examination",
      question: "9. Performs a physical therapy patient Examination using evidenced-based tests and measures.",
    },
    showContent3: {
      name: "Patient Management – Evaluation",
      question: "10. Evaluates data from the patient examination (history, systems review, and tests and measures) to make clinical judgments.",
    },
    showContent4: {
      name: "Patient Management – Diagnosis and Prognosis",
      question: "11. Determines a Diagnosis and Prognosis that guides future patient management.",
    },
    showContent5: {
      name: "Patient Management – Plan of Care",
      question: "12. Establishes a physical therapy Plan of Care that is safe, effective, patient-centered, and evidence-based.",
    },
    showContent6: {
      name: "Patient Management – Procedural Interventions",
      question: "13. Performs physical therapy Interventions in a competent manner.",
    },
    showContent7: {
      name: "Patient Management – Educational Interventions",
      question: "14. Educates others (patients, caregivers, staff, students, other health care providers, business and industry representatives, school systems) using relevant and effective teaching methods.",
    },
    showContent8: {
      name: "Patient Management – Documentation",
      question: "15. Produces quality Documentation in a timely manner to support the delivery of physical therapy services.",
    },
    showContent9: {
      name: "Patient Management – Outcomes Assessment",
      question: "16. Collects and analyzes data from selected outcome measures in a manner that supports accurate analysis of individual patient and group outcomes.",
    },
    showContent10: {
      name: "Patient Management – Financial Resources",
      question: "17. Participates in the financial management (budgeting, billing and reimbursement, time, space, equipment, marketing, public relations) of the physical therapy service consistent with regulatory, legal, and facility guidelines.",
    },
    showContent11: {
      name: "Patient Management – Direction and Supervision of Personnel",
      question: "18. Directs and supervises personnel to meet patient's goals and expected outcomes according to legal standards and ethical guidelines.",
    },
  }


  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const emails = ['username@gmail.com', 'user02@gmail.com'];

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };
  const [selectedValue, setSelectedValue] = React.useState(emails[1]);

  // const [showContent, setShowContent] = React.useState(false)
  // const onClickSetShowContent = (value) => setShowContent(value)
  const [width, setWidth]   = React.useState(window.innerWidth);
  const updateDimensions = () => {
    setWidth(window.innerWidth);
    // setHeight(window.innerHeight);
  }

  React.useEffect(() => {
    window.addEventListener("resize", updateDimensions);
    return () => window.removeEventListener("resize", updateDimensions);
  }, []);

  const [page, setPage] = React.useState(1);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <div className={classes.root}>
    <div className="ptcpi-evaluation-heading-main">
      <div>
        <div className="ptcpi-evaluation-heading">
          Physical Therapy Clinical Performance Instrument
        </div>
      </div>
      <div className="evaluation-heading-div">
        <div className="evaluation-heading">
          Student:
        </div>
        <div className="evaluation-heading-content">
          Jennifer Avery, PT
        </div>
      </div>
      <div className="evaluation-heading-div">
        <div className="evaluation-heading">
          Clinical Staff:
        </div>
        <div className="evaluation-heading-content">
          Lorayne Castiglione
        </div>
      </div>
      <div className="evaluation-heading-div">
        <div className="evaluation-heading">
          Site:
        </div>
        <div className="evaluation-heading-content">
        Hillside School
        </div>
      </div>
      <div className="evaluation-heading-div">
        <div className="evaluation-heading">
          Evaluation Name:
        </div>
        <div className="evaluation-heading-content">
        PHT 88950 Class of 2015
        </div>
      </div>
    </div>
      {/* {questionSection2(width,allValues1,onClickSetShowContent1,section2,open,handleClose,handleClickOpen)} */}
        {/* <Slider1
        autoplay = {false}
        renderControls={(next, previous) => [
          <div className="slider_button_div">
            <Button onClick={previous} variant="contained" color="primary">Previous</Button>,
            <Button onClick={next} variant="contained" color="primary">Next</Button>
          </div>
        ]}>
          {questionSection1(width,allValues,onClickSetShowContent,open,handleClose,handleClickOpen)}
          {questionSection2(width,allValues1,onClickSetShowContent1,section2,open,handleClose,handleClickOpen)}
      </Slider1> */}
      {/* <Slide direction={page===1 ? "right" : "left"}  in={true} timeout={1000} > */}
      <div>
        {/* {page === 1 ? questionSection1(width,allValues,onClickSetShowContent,open,handleClose,handleClickOpen,page) : questionSection2(width,allValues1,onClickSetShowContent1,section2,open,handleClose,handleClickOpen,page) } */}
        <Slide direction="right"  in={page===1} timeout={{appear: 100, enter: 1000,  exit: 100} } mountOnEnter unmountOnExit style = {{display: page!==1 ? "none" : ""}} >
          {questionSection1(width,allValues,onClickSetShowContent,open,handleClose,handleClickOpen,page)}
        </Slide>
        <Slide direction="left"  in={page === 2} timeout={{appear: 100, enter: 1000,  exit: 100} } mountOnEnter unmountOnExit
        style = {{display: page!==2 ? "none" : ""}} >
          {questionSection2(width,allValues1,onClickSetShowContent1,section2,open,handleClose,handleClickOpen,page)}
        </Slide>
      </div>
      {/* </Slide> */}
      <Pagination count={2} color="primary" page={page} onChange={handlePageChange}/>
    </div>
  );
}

function questionSection1(width,allValues,onClickSetShowContent,open,handleClose,handleClickOpen,page){
  // return <Slide direction="right"  in={page===1} timeout={1000} mountOnEnter unmountOnExit >
    return <div className="main_div" >
      <div className="main_heading" style={{display: width <= 574 ? 'none' : '',fontWeight: "bolder"}}>
          <div className="question_block" >
            <div className="question_block_status">
              <div className="question_block_status_row" style={{backgroundColor: '#043363', color: "white"}}>
                <div
                  className = "question_block_status_row_div"
                  style={{
                    width: width <= 574 ? '100%' : '30%',
                    textAlign: width <= 574 ? 'center' : 'inherit',
                    paddingTop: width <= 574 ? '12px' : '',
                    fontWeight: "bolder"
                  }}
                  // onClick={width <= 574 ? () => onClickSetShowContent("showContent") : () => {}}
                >
                  Performance Criterion / Selection
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex',fontWeight: "bolder"}}>
                  Status
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex',fontWeight: "bolder"}}>
                  Rating
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex',fontWeight: "bolder"}}>
                  Comment Entered
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex',fontWeight: "bolder"}}>
                  Last Updated
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex',fontWeight: "bolder"}}>
                  <div
                  // onClick={() => onClickSetShowContent("showContent")}
                  >
                    Action
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="question_block" >
          <div className="question_block_status">
            <div className="question_block_status_row" style={{backgroundColor: '#3CB371', color: "black"}}>
              <div
                className = "question_block_status_row_div"
                style={{
                  width: width <= 574 ? '100%' : '30%',
                  textAlign: width <= 574 ? 'center' : 'inherit',
                  paddingTop: width <= 574 ? '12px' : '',
                }}
                onClick={width <= 574 ? () => onClickSetShowContent("showContent") : () => {}}
              >
                Professional Practice – Safety
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%'}}>
                Completed
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                15
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                YES
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                06/03/21 11:09 AM
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                <div onClick={() => onClickSetShowContent("showContent")}>
                  Hide/Show
                </div>
              </div>
            </div>
          </div>
            <Collapse in={allValues.showContent} timeout={600}>
              <div className="question_block_content" id="question_block_content">
              <div className="question-heading_div">
                <Typography id="question-heading" >
                  Professional Practice – Safety
                </Typography>
                <Typography id="question-heading-sample-behaviour" onClick={handleClickOpen} >
                View Sample Behaviour
                </Typography>
                <SimpleDialog  open={open} onClose={handleClose} />
              </div>
                <Typography id="discrete-question" className="discrete-question" >
                  1. Practices in a safe manner that minimizes the risk to patient, self, and others
                </Typography>
                <Typography id="discrete-slider-always" className ="discrete-slider-always">
                  Select a Rating
                </Typography>
                <Slider
                  ValueLabelComponent={ValueLabelComponent}
                  defaultValue={15}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider-always"
                  valueLabelFormat={valuetext}
                  // step={100}
                  marks = {width <= 574 ? true : marks}
                  valueLabelDisplay="auto"
                  min={0}
                  max={20}
                />
                <TextareaAutosize
                  aria-label="empty textarea"
                  placeholder="Enter your Response/Reason"
                  className="textarea_class"
                  rowsMin={10}
                  rowsMax={10}
                  defaultValue="Since the midterm, I believe I have continued to establish and maintain the safest treatment and working environment for my patients and my coworkers. I sill work on using correct body mechanics in carrying, lifting, and guarding my sizes of my pediatric patients. I continue to ask  questions or help, I never hesitate to ask my CI for assistance. I always don and doff my patients braces in a safe and pain-free manner to both the student and myself. I will continue to work on and improve on my safety techniques."
                />
              </div>
            </Collapse>
        </div>
        <div className="question_block">
          <div className="question_block_status">
            <div className="question_block_status_row" style={{backgroundColor: 'rgb(221, 223, 230)', color: "black"}}>
              <div
                className = "question_block_status_row_div"
                style={{
                  width: width <= 574 ? '100%' : '30%',
                  textAlign: width <= 574 ? 'center' : 'inherit',
                  paddingTop: width <= 574 ? '12px' : '',
                }}
                onClick={width <= 574 ? () => onClickSetShowContent("showContent1")  : () => {}}
              >
                Professional Practice – Professional Behavior
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%'}}>
                Incomplete
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                15
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                NO
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                06/03/21 11:09 AM
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                <div onClick={() => onClickSetShowContent("showContent1")}>
                  Hide/Show
                </div>
              </div>
            </div>
          </div>
          <Collapse in={allValues.showContent1} timeout={600}>
            <div className="question_block_content" id="question_block_content">
            <div className="question-heading_div">
              <Typography id="question-heading" >
              Professional Practice – Professional Behavior
              </Typography>
              <Typography id="question-heading-sample-behaviour" onClick={handleClickOpen} >
              View Sample Behaviour
              </Typography>
              <SimpleDialog  open={open} onClose={handleClose} />
            </div>
              <Typography id="discrete-question" className="discrete-question" >
                2. Demonstrates professional behavior in all situations.
              </Typography>
              <Typography id="discrete-slider-always" className ="discrete-slider-always">
                Select a Rating
              </Typography>
              <Slider
                ValueLabelComponent={ValueLabelComponent}
                defaultValue={15}
                getAriaValueText={valuetext}
                aria-labelledby="discrete-slider-always"
                valueLabelFormat={valuetext}
                // step={100}
                marks = {width <= 574 ? true : marks}
                valueLabelDisplay="auto"
                min={0}
                max={20}
              />
              <TextareaAutosize
                aria-label="empty textarea"
                placeholder="Enter your Response/Reason"
                className="textarea_class"
                rowsMin={10}
              />
            </div>
          </Collapse>
        </div>
        <div className="question_block">
          <div className="question_block_status">
            <div className="question_block_status_row" style={{backgroundColor: 'rgb(221, 223, 230)', color: "black"}}>
              <div
                className = "question_block_status_row_div"
                style={{
                  width: width <= 574 ? '100%' : '30%',
                  textAlign: width <= 574 ? 'center' : 'inherit',
                  paddingTop: width <= 574 ? '12px' : '',
                }}
                onClick={width <= 574 ? () => onClickSetShowContent("showContent2")  : () => {}}
              >
                Professional Practice – Accountability
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%'}}>
                Incomplete
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                15
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                NO
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                06/03/21 11:09 AM
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                <div onClick={() => onClickSetShowContent("showContent2")}>
                  Hide/Show
                </div>
              </div>
            </div>
          </div>
          <Collapse in={allValues.showContent2} timeout={600}>
              <div className="question_block_content" id="question_block_content">
              <div className="question-heading_div">
                <Typography id="question-heading" >
                Professional Practice – Accountability
                </Typography>
                <Typography id="question-heading-sample-behaviour" onClick={handleClickOpen} >
                View Sample Behaviour
                </Typography>
                <SimpleDialog  open={open} onClose={handleClose} />
              </div>
                <Typography id="discrete-question" className="discrete-question" >
                  3. Practices in a manner consistent with established legal and professional standards and ethical guidelines.
                </Typography>
                <Typography id="discrete-slider-always" className ="discrete-slider-always">
                  Select a Rating
                </Typography>
                <Slider
                  ValueLabelComponent={ValueLabelComponent}
                  defaultValue={15}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider-always"
                  valueLabelFormat={valuetext}
                  // step={100}
                  marks = {width <= 574 ? true : marks}
                  valueLabelDisplay="auto"
                  min={0}
                  max={20}
                />
                <TextareaAutosize
                  aria-label="empty textarea"
                  placeholder="Enter your Response/Reason"
                  className="textarea_class"
                  rowsMin={10}
                />
              </div>
          </Collapse>
        </div>
        <div className="question_block">
          <div className="question_block_status">
            <div className="question_block_status_row" style={{backgroundColor: 'rgb(221, 223, 230)', color: "black"}}>
              <div
                className = "question_block_status_row_div"
                style={{
                  width: width <= 574 ? '100%' : '30%',
                  textAlign: width <= 574 ? 'center' : 'inherit',
                  paddingTop: width <= 574 ? '12px' : '',
                }}
                onClick={width <= 574 ? () => onClickSetShowContent("showContent3")  : () => {}}
              >
                Professional Practice – Communication
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%'}}>
                Incomplete
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                15
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                NO
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                06/03/21 11:09 AM
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                <div onClick={() => onClickSetShowContent("showContent3")}>
                  Hide/Show
                </div>
              </div>
            </div>
          </div>
          <Collapse in={allValues.showContent3} timeout={600}>

          <div className="question_block_content" id="question_block_content">
              <div className="question-heading_div">
                <Typography id="question-heading" >
                Professional Practice – Communication
                </Typography>
                <Typography id="question-heading-sample-behaviour" onClick={handleClickOpen} >
                View Sample Behaviour
                </Typography>
                <SimpleDialog  open={open} onClose={handleClose} />
              </div>
                <Typography id="discrete-question" className="discrete-question" >
                  4. Communicates in ways that are congruent with situational needs.
                </Typography>
                <Typography id="discrete-slider-always" className ="discrete-slider-always">
                  Select a Rating
                </Typography>
                <Slider
                  ValueLabelComponent={ValueLabelComponent}
                  defaultValue={15}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider-always"
                  valueLabelFormat={valuetext}
                  // step={100}
                  marks = {width <= 574 ? true : marks}
                  valueLabelDisplay="auto"
                  min={0}
                  max={20}
                />
                <TextareaAutosize
                  aria-label="empty textarea"
                  placeholder="Enter your Response/Reason"
                  className="textarea_class"
                  rowsMin={10}
                />
              </div>
          </Collapse>
        </div>
        <div className="question_block">
          <div className="question_block_status">
            <div className="question_block_status_row" style={{backgroundColor: 'rgb(221, 223, 230)', color: "black"}}>
              <div
                className = "question_block_status_row_div"
                style={{
                  width: width <= 574 ? '100%' : '30%',
                  textAlign: width <= 574 ? 'center' : 'inherit',
                  paddingTop: width <= 574 ? '12px' : '',
                }}
                onClick={width <= 574 ? () => onClickSetShowContent("showContent4")  : () => {}}
              >
                Professional Practice – Cultural Competence
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%'}}>
                Incomplete
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                15
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                NO
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                06/03/21 11:09 AM
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                <div onClick={() => onClickSetShowContent("showContent4")}>
                  Hide/Show
                </div>
              </div>
            </div>
          </div>
          <Collapse in={allValues.showContent4} timeout={600}>
              <div className="question_block_content" id="question_block_content">
              <div className="question-heading_div">
                <Typography id="question-heading" >
                  Professional Practice – Cultural Competence
                </Typography>
                <Typography id="question-heading-sample-behaviour" onClick={handleClickOpen} >
                View Sample Behaviour
                </Typography>
                <SimpleDialog  open={open} onClose={handleClose} />
              </div>
                <Typography id="discrete-question" className="discrete-question" >
                  5. Adapts delivery of physical therapy services with consideration for patient differences, values, preferences, and needs.
                </Typography>
                <Typography id="discrete-slider-always" className ="discrete-slider-always">
                  Select a Rating
                </Typography>
                <Slider
                  ValueLabelComponent={ValueLabelComponent}
                  defaultValue={15}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider-always"
                  valueLabelFormat={valuetext}
                  // step={100}
                  marks = {width <= 574 ? true : marks}
                  valueLabelDisplay="auto"
                  min={0}
                  max={20}
                />
                <TextareaAutosize
                  aria-label="empty textarea"
                  placeholder="Enter your Response/Reason"
                  className="textarea_class"
                  rowsMin={10}
                />
              </div>
          </Collapse>
        </div>
        <div className="question_block">
          <div className="question_block_status">
            <div className="question_block_status_row" style={{backgroundColor: 'rgb(221, 223, 230)', color: "black"}}>
              <div
                className = "question_block_status_row_div"
                style={{
                  width: width <= 574 ? '100%' : '30%',
                  textAlign: width <= 574 ? 'center' : 'inherit',
                  paddingTop: width <= 574 ? '12px' : '',
                }}
                onClick={width <= 574 ? () => onClickSetShowContent("showContent5")  : () => {}}
              >
                Professional Practice – Professional Development
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%'}}>
                Incomplete
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                15
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                NO
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                06/03/21 11:09 AM
              </div>
              <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                <div onClick={() => onClickSetShowContent("showContent5")}>
                  Hide/Show
                </div>
              </div>
            </div>
          </div>
          <Collapse in={allValues.showContent5} timeout={600}>
            <div className="question_block_content" id="question_block_content">
              <div className="question-heading_div">
                <Typography id="question-heading" >
                Professional Practice – Professional Development
                </Typography>
                <Typography id="question-heading-sample-behaviour" onClick={handleClickOpen} >
                View Sample Behaviour
                </Typography>
                <SimpleDialog  open={open} onClose={handleClose} />
              </div>
                <Typography id="discrete-question" className="discrete-question" >
                  6. Participates in self-assessment to improve clinical and professional performance.
                </Typography>
                <Typography id="discrete-slider-always" className ="discrete-slider-always">
                  Select a Rating
                </Typography>
                <Slider
                  ValueLabelComponent={ValueLabelComponent}
                  defaultValue={15}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider-always"
                  valueLabelFormat={valuetext}
                  // step={100}
                  marks = {width <= 574 ? true : marks}
                  valueLabelDisplay="auto"
                  min={0}
                  max={20}
                />
                <TextareaAutosize
                  aria-label="empty textarea"
                  placeholder="Enter your Response/Reason"
                  className="textarea_class"
                  rowsMin={10}
                />
              </div>
          </Collapse>
        </div>
    </div>
  // </Slide>
}

function questionSection2(width,allValues1,onClickSetShowContent1,section2,open,handleClose,handleClickOpen,page){
  // return <Slide direction="left"  in={page === 2} timeout={1000}  mountOnEnter unmountOnExit >
    return <div className="main_div">
      <div className="main_heading" style={{display: width <= 574 ? 'none' : '',fontWeight: "bolder"}}>
          <div className="question_block" >
            <div className="question_block_status">
              <div className="question_block_status_row" style={{backgroundColor: '#043363', color: "white"}}>
                <div
                  className = "question_block_status_row_div"
                  style={{
                    width: width <= 574 ? '100%' : '30%',
                    textAlign: width <= 574 ? 'center' : 'inherit',
                    paddingTop: width <= 574 ? '12px' : '',
                    fontWeight: "bolder"
                  }}
                  // onClick={width <= 574 ? () => onClickSetShowContent("showContent") : () => {}}
                >
                  Performance Criterion / Selection
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%',fontWeight: "bolder"}}>
                  Status
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%',fontWeight: "bolder"}}>
                  Rating
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%',fontWeight: "bolder"}}>
                  Comment Entered
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%',fontWeight: "bolder"}}>
                  Last Updated
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%',fontWeight: "bolder"}}>
                  <div
                  // onClick={() => onClickSetShowContent1("showContent")}
                  >
                    Action
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      {
        Object.keys(allValues1).map((key,i) => {
          return <div className="question_block">
            <div className="question_block_status">
              <div className="question_block_status_row" style={{backgroundColor:  section2[key].name === "Patient Management – Evaluation" ? '#3CB371' : 'rgb(221, 223, 230)' , color: "black"}}>
                <div
                  className = "question_block_status_row_div"
                  style={{
                    width: width <= 574 ? '100%' : '30%',
                    textAlign: width <= 574 ? 'center' : 'inherit',
                    paddingTop: width <= 574 ? '12px' : '',
                  }}
                  onClick={width <= 574 ? () => onClickSetShowContent1(key) : () => {}}
                >
                  {console.log(section2[key].name)}
                  {section2[key].name}
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '30%', display: width <= 574 ? 'none' : 'flex%'}}>
                  {/* Incomplete */}
                  {section2[key].name === "Patient Management – Evaluation" ? "Complete" : "Incomplete"}
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                  15
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                  {section2[key].name === "Patient Management – Evaluation" ? "YES" : "NO"}
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                  06/03/21 11:09 AM
                </div>
                <div className = "question_block_status_row_div" style={{width: width <= 574 ? '50%' : '10%', display: width <= 574 ? 'none' : 'flex%'}}>
                  <div onClick={() => onClickSetShowContent1(key)}>
                    Hide/Show
                  </div>
                </div>
              </div>
            </div>
            {/* { */}
              {/* allValues1[key] ? */}
              <Collapse in={allValues1[key]} timeout={600}>
                <div className="question_block_content" id="question_block_content">
                <div className="question-heading_div">
                  <Typography id="question-heading" >
                  {section2[key].name}
                  </Typography>
                  <Typography id="question-heading-sample-behaviour" onClick={handleClickOpen} >
                  View Sample Behaviour
                  </Typography>
                  <SimpleDialog  open={open} onClose={handleClose} />
                </div>

                  <Typography id="discrete-question" className="discrete-question" >
                    {section2[key].question}
                  </Typography>
                  <Typography id="discrete-slider-always" className ="discrete-slider-always">
                    Select a Rating
                  </Typography>
                  <Slider
                    ValueLabelComponent={ValueLabelComponent}
                    defaultValue={15}
                    getAriaValueText={valuetext}
                    aria-labelledby="discrete-slider-always"
                    valueLabelFormat={valuetext}
                    // step={100}
                    marks = {width <= 574 ? true : marks}
                    valueLabelDisplay="auto"
                    min={0}
                    max={20}
                  />
                  <TextareaAutosize
                    aria-label="empty textarea"
                    placeholder="Enter your Response/Reason"
                    className="textarea_class"
                    rowsMin={10}
                  />
                </div>
                </Collapse>
              {/* : "" */}
            {/* } */}
          </div>
        })
      }
    </div>
  // </Slide>
}

function SimpleDialog(props) {
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleListItemClick = (value) => {
    onClose(value);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">Sample Behaviors</DialogTitle>
      <List>
        <ListItem>
          <ListItemText primary="Determines those physical therapy services that can be directed to other support personnel according to jurisdictional law, practice guidelines, policies, codes of ethics, and facility policies." />
        </ListItem>
        <ListItem>
          <ListItemText primary="Applies time-management principles to supervision and patient care." />
        </ListItem>

        <ListItem>
          <ListItemText primary="Informs the patient of the rationale for and decision to direct aspects of physical therapy services to support personnel (eg, secretary, volunteers, PT Aides, Physical Therapist Assistants).
" />
        </ListItem>

        <ListItem>
          <ListItemText primary="Determines the amount of instruction necessary for personnel to perform directed tasks.
" />
        </ListItem>

        <ListItem>
          <ListItemText primary="Provides instruction to personnel in the performance of directed tasks.
" />
        </ListItem>

        <ListItem>
          <ListItemText primary="Supervises those physical therapy services directed to physical therapist assistants and other support personnel according to jurisdictional law, practice guidelines, policies, codes of ethics, and facility policies.
" />
        </ListItem>

        <ListItem>
          <ListItemText primary="Monitors the outcomes of patients receiving physical therapy services delivered by other support personnel.
" />
        </ListItem>

        <ListItem>
          <ListItemText primary="Demonstrates effective interpersonal skills including regular feedback in supervising directed support personnel." />
        </ListItem>

        <ListItem>
          <ListItemText primary="Directs documentation to physical therapist assistants that is based on the plan of care that is within the physical therapist assistant's ability and consistent with jurisdictional law, practice guidelines, policies, codes of ethics, and facility policies." />
        </ListItem>

        <ListItem>
          <ListItemText primary="Reviews, in conjunction with the clinical instructor, physical therapist assistant documentation for clarity and accuracy." />
        </ListItem>
      </List>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
};

const renderView = ({ index, active, transitionState }) => (
  <div>
    <h3>View {index}</h3>
    <p>I am {active ? 'active' : 'inactive'}</p>
    <p>transitionState: {transitionState}</p>
  </div>
)

export default Evaluation;
import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import StarBorder from '@material-ui/icons/StarBorder';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ptlogo from '../logos/apta-ptcpi-logo.png';
import ptalogo from '../logos/apta-ptacpi-logo.png';
import useStyles from '../styles/navigation'
import AppBarComponent from '../components/appbar'


// const drawerWidth = 240;

// const useStyles = userStyles;

function ResponsiveDrawer(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  // const [open, setOpen] = React.useState(true);

  // const handleClick = () => {
  //   setOpen(!open);
  // };


  const [allValues, setAllValues] = React.useState({
    open: false,
    program_open: false,
    program_staff: false,
    site: false,
    scce_ci_open: false,
    eval_setup_open: false
 });
 const changeHandler = (key) => {
    setAllValues({...allValues, [key]: !allValues[key]})
 }

  const [ptalogo1, setPtaLogo] = React.useState(false);

  const handlelogo = (value) => {
    setPtaLogo(value);
  };

  const drawer = (
    <div>
      <Divider />
        {/* {['Dashboard', 'Program', 'Program Staff', 'Site', 'SCCE/CI', 'Eval Setup', 'Evaluations', 'Critical Incidents'].map((text, index) => (
        <List>
          <ListItem button key={text}>
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
          <Divider />
        </List>
        ))} */}

        <List>
          <ListItem button key="Dashboard">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
          <Divider />
        </List>

        <List>
          <ListItem button key="Program">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Program" />
            {allValues.program_open ? <ExpandLess name="program_open" onClick={() => changeHandler("program_open")}/> : <ExpandMore name = "program_open" onClick={() =>changeHandler("program_open")}/>}
          </ListItem>
          <Collapse in={allValues.program_open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                    <ListItemIcon>
                    <StarBorder />
                    </ListItemIcon>
                    <ListItemText primary="New" />
                </ListItem>
            </List>
          </Collapse>
          <Divider />
        </List>

        <List>
          <ListItem button key="Program Staff">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Program Staff" />
            {allValues.program_staff ? <ExpandLess name="program_staff" onClick={() => changeHandler("program_staff")}/> : <ExpandMore name = "program_staff" onClick={() => changeHandler("program_staff")}/>}
          </ListItem>
          <Collapse in={allValues.program_staff} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                    <ListItemIcon>
                    <StarBorder />
                    </ListItemIcon>
                    <ListItemText primary="New" />
                </ListItem>
            </List>
          </Collapse>
          <Divider />
        </List>


        <List>
          <ListItem button key="Site">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Site" />
            {allValues.site ? <ExpandLess name="site" onClick={() => changeHandler("site")}/> : <ExpandMore name = "site" onClick={() => changeHandler("site")}/>}
          </ListItem>
          <Collapse in={allValues.site} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                    <ListItemIcon>
                    <StarBorder />
                    </ListItemIcon>
                    <ListItemText primary="New" />
                </ListItem>
            </List>
          </Collapse>
          <Divider />
        </List>

        <List>
          <ListItem button key="SCCE/CI">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="SCCE/CI" />
            {allValues.scce_ci_open ? <ExpandLess name="scce_ci_open" onClick={() => changeHandler("scce_ci_open")}/> : <ExpandMore name = "scce_ci_open" onClick={() => changeHandler("scce_ci_open")}/>}
          </ListItem>
          <Collapse in={allValues.scce_ci_open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                    <ListItemIcon>
                    <StarBorder />
                    </ListItemIcon>
                    <ListItemText primary="New" />
                </ListItem>
            </List>
          </Collapse>
          <Divider />
        </List>


        <List>
          <ListItem button key="Eval Setup">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Eval Setup" />
            {allValues.eval_setup_open ? <ExpandLess name="eval_setup_open" onClick={() => changeHandler("eval_setup_open")}/> : <ExpandMore name = "eval_setup_open" onClick={() => changeHandler("eval_setup_open")}/>}
          </ListItem>
          <Collapse in={allValues.eval_setup_open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                <ListItem button className={classes.nested}>
                    <ListItemIcon>
                    <StarBorder />
                    </ListItemIcon>
                    <ListItemText primary="New" />
                </ListItem>
            </List>
          </Collapse>
          <Divider />
        </List>

        <List>
          <ListItem button key="Evaluations">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Evaluations" />
          </ListItem>
          <Divider />
        </List>

        <List>
          <ListItem button key="Critical Incidents">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Critical Incidents" />
          </ListItem>
          <Divider />
        </List>

        <List>
          <ListItem button key="Field">
            <ListItemIcon><MailIcon /></ListItemIcon>
            <ListItemText primary="Field" />
            {allValues.open ? <ExpandLess name="open" onClick={() => changeHandler("open")}/> : <ExpandMore name = "open" onClick={() => changeHandler("open")}/>}
          </ListItem>
          <Collapse in={allValues.open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem button className={classes.nested} onClick={() => handlelogo(false)}>
                        <ListItemIcon>
                        <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="PT" />
                    </ListItem>
                    <ListItem button className={classes.nested} onClick={() =>handlelogo(true)}>
                        <ListItemIcon>
                        <StarBorder />
                        </ListItemIcon>
                        <ListItemText primary="PTA" />
                    </ListItem>
                </List>
            </Collapse>
            <Divider />
        </List>
    </div>
  );
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBarComponent ptalogo_value = {ptalogo1} classes = {classes} toggle = {handleDrawerToggle} mobileOpen = {mobileOpen}/>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            // container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            className="hide_drawer"
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        {props.content}
      </main>
    </div>
  );
}

export default ResponsiveDrawer;
import React from 'react';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
// import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';



function handleClick(event) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

const useStyles = makeStyles((theme) => ({
  root: {
    // display: 'flex',
    flexWrap: 'wrap',
    width: "100%",
    margin: "36px 0px"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '30%',
  },
}));

function NewProgramComponent() {
  const classes = useStyles();
  const [value, setValue] = React.useState('pt');

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const [csifvalue, setcsifValue] = React.useState('no');

  const handleCSIFChange = (event) => {
    setcsifValue(event.target.value);
  };
  return (
    <div className="program_main">
      <div className="breadcrumbs">
        <Breadcrumbs aria-label="breadcrumb">
          <Link color="inherit" href="/" onClick={handleClick}>
            Dashboard
          </Link>
          <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}>
            Program
          </Link>
          <Typography color="textPrimary">New</Typography>
        </Breadcrumbs>
      </div>
      <div className="title">
        NEW PROGRAM
      </div>
      <div className="content">
        <form className={classes.root} noValidate autoComplete="off">
        <div className={classes.root}>
          <div>
            <TextField
              id="standard-full-width"
              label="Program Name"
              style={{ margin: "20px 8px" }}
              placeholder="Enter Program Name"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <FormControl component="fieldset" style={{margin: "20px 8px"}} className= "radio_width first_width">
              <FormLabel component="legend">Field</FormLabel>
              <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}
                style={{display: "block"}}>
              <FormControlLabel value="pt" control={<Radio />} label="PT" style = {{ width: "40%"}}/>
              <FormControlLabel value="pta" control={<Radio />} label="PTA" style = {{ width: "40%"}}/>
              </RadioGroup>
            </FormControl>


            <FormControl component="fieldset" style={{margin: "20px 8px"}} className= "radio_width first_width">
              <FormLabel component="legend">CSIF Enabled</FormLabel>
              <RadioGroup aria-label="gender" name="gender1" value={csifvalue} onChange={handleCSIFChange}
                style={{display: "block"}}>
              <FormControlLabel value="yes" control={<Radio />} label="Yes" style = {{ width: "40%"}}/>
              <FormControlLabel value="no" control={<Radio />} label="No" style = {{ width: "40%"}}/>
              </RadioGroup>
            </FormControl>

            <TextField
              label="Main Phone"
              id="margin-none"
              className={classes.textField ,"second_width"}
              style={{ margin: "20px 8px" }}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              label="Alternate Phone"
              id="margin-dense"
              className={classes.textField, "first_width"}
              // margin="dense"
              style={{ margin: "20px 8px"}}
            />
            <TextField
              label="Fax Number"
              id="margin-normal"
              className={classes.textField,"second_width"}
              // margin="normal"
              style={{ margin: "20px 8px"}}
            />
          </div>
          <div>
            <TextField
              id="filled-full-width"
              label="Main Address"
              style={{ margin: "20px 8px" }}
              placeholder="Address Line 1"
              fullWidth
              // margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="filled"
            />
            <TextField
              label="Address Line 2"
              id="filled-margin-none"
              className={classes.textField, "full_width"}
              variant="filled"
              style={{ margin: "20px 8px" }}
            />
            <TextField
              label="Address Line 3"
              id="filled-margin-dense"
              className={classes.textField, "full_width"}
              // margin="dense"
              variant="filled"
              style={{ margin: "20px 8px" }}
            />
            <TextField
              label="City"
              id="filled-margin-normal"
              className={classes.textField, "full_width"}
              // margin="normal"
              variant="filled"
              style={{ margin: "20px 8px" }}
            />
          </div>
          <div>
            <TextField
              label="State"
              id="outlined-margin-none"
              className={classes.textField , "full_width"}
              variant="outlined"
              style={{ margin: "20px 8px" }}
            />
            <TextField
              label="Postal Code"
              id="outlined-margin-dense"
              className={classes.textField, "full_width"}
              // margin="dense"
              variant="outlined"
              style={{ margin: "20px 8px" }}
            />
            <TextField
              label="Country"
              id="outlined-margin-normal"
              className={classes.textField, "full_width"}
              // margin="normal"
              variant="outlined"
              style={{ margin: "20px 8px" }}
            />
          </div>
        </div>
        <div className="button_div">
          <Button variant="contained" color="primary">
            Submit
          </Button>
        </div>
        </form>
      </div>
    </div>
  );
}

export default NewProgramComponent;
import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import useStyles from '../styles/navigation'
import ptlogo from '../logos/apta-ptcpi-logo.png';
import ptalogo from '../logos/apta-ptacpi-logo.png';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';

function AppBarComponent(props) {
    console.log("sasassssasa")
    // const classes = props.classes
    const classes = useStyles();
    const theme = useTheme();

    // const [mobileOpen, setMobileOpen] = React.useState(false);

    // const handleDrawerToggle = () => {
        // setMobileOpen(!mobileOpen);
    // };


    return (
        <AppBar position="fixed" className={props.ptalogo_value ? "PTAMuiAppBar-colorPrimary" : classes.appBar}>
            <Toolbar>
            <IconButton
                color="inherit"
                aria-label="open drawer"
                edge="start"
                onClick={props.toggle}
                className={classes.menuButton}
            >
                <MenuIcon />
            </IconButton>
            {/* <div > */}
                <img src={props.ptalogo_value ? ptalogo : ptlogo} alt="logo" className="logo_div"/>
            {/* </div> */}
            <Hidden xsDown implementation="css" className="hidden_app_name_div">
                <Typography variant="h6" noWrap className= {props.mobileOpen ? "hidden" : "none"}>
                    CLINICAL PERFORMANCE INSTRUMENT
                </Typography>

            </Hidden>
            </Toolbar>
        </AppBar>
    )
}


export default AppBarComponent;
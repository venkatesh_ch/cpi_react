function Layout(props) {
    return (
      <div className="layout">
        <div className="top">{props.top}</div>
        <div className="left">{props.left}</div>
        <div className="center">{props.center}</div>
      </div>
    );
}